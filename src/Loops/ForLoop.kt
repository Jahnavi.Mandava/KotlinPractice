package Loops
//print the odd numbers by using for loop
fun main(args:Array<String>){
    for(i in 1..10){
        if(i%2!==0){
            println(i)
        }
    }
}