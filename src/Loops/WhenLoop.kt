package Loops

// 1st model
fun main(args:Array<String>){
    var x = 2
    when(x){
        1->println("x is 1")
        2->println("x is 2")
        else->println("x value is unknown")
    }

}

//2nd model
fun main(args:Array<String>){
    var x =5
    when(x){
        1->{
          println("x is 1")
        }
        2->{
            println("x is 2")
        }
        else->{
            println("x value is unknown")
            }
    }
}

//3rd model
fun main(args:Array<String>){
    var x = 2
    when(x){
        in 1..2->{
            println("x is 1 or 2")
        }
        in 3..10->{
            println("x lies 3 to 10")
        }
        else->{
            println("x value is unknown")
        }
    }
}

//4th model
fun main(args:Array<String>){
    var x = 99
    var str:String = when (x){
        1->"x is 1"
        2->"x is 2"
        else->{
            "x value is unknown"
        }
    }
    println(str)
}