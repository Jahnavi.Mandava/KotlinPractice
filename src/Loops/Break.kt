package Loops
//break example with labelled for loop
fun main(args:Array<String>){
    myLoop@ for(i in 1..5){
        for(j in 1..5){
            println("$i $j")
            if(i==2&&j==2)
                break@myLoop
        }
    }
}