class P(var name: String, var address: String, var id: Int){
    override fun toString(): String {
        return "Person[name=$name,address=$address,id=$id]"
    }
}
data class DataP(var name: String, var address: String, var id: Int){
    fun print(){
        println()
    }
}

fun main(args:Array<String>) {

    val p = P("jahnavi","tenali",4)
    val p2 = P("siva","guntur",2)
    val dataP = DataP("dhana","vijayawada",6)
    val dataP2 = DataP("roja","ongole",8)
    val dataP3 = dataP.copy(id=10)
    val(name,address,id)=dataP
    val set = hashSetOf(dataP,dataP2,dataP3)
    val set2 = hashSetOf(p,p2)
    println(set)
    println(set2)
    println(p)
    println(dataP)
    println(dataP3)
    println(name)
    println(address)
    println(p.equals(p2))
    println(dataP.equals(dataP2))


}

