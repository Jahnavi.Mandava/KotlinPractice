enum class Direction(degree:Double){
    North(0.0),East(90.0),south(180.0),north(270.0)
}
enum class Suits{
    HEARTS,SPADES,DIAMONDS,CLUBS
}
fun main(args:Array<String>){
    val suit = Suits.SPADES

    val color = when(suit){
        Suits.HEARTS, Suits.DIAMONDS->"red"
        Suits.SPADES, Suits.CLUBS->"black"
    }
    println(color)
}