package com.example.jahnavimandava.myapplicationja

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner



class MainActivity : AppCompatActivity() {
    val distance = ArrayList<String>()
    val pricebetween = ArrayList<String>()
    val pricebetween1 = ArrayList<String>()
    val bedrooms = ArrayList<String>()
    val bedrooms1 = ArrayList<String>()
    val furnished = ArrayList<String>()
    val sortlistingsby = ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        distance.add("within 5 miles")
        distance.add("within 10 miles")
        distance.add("within 15 miles")
        distance.add("within 20 miles")
        pricebetween.add("300 pw")
        pricebetween.add("500 pw")
        pricebetween.add("800 pw")
        pricebetween1.add("650 pw")
        pricebetween1.add("700 pw")
        pricebetween1.add("750 pw")
        bedrooms.add("1 bedroom")
        bedrooms.add("2 bedrooms")
        bedrooms.add("3 bedrooms")
        bedrooms.add("4 bedrooms")
        bedrooms1.add("No max")
        bedrooms1.add("Min 20lakh")
        furnished.add("furnished")
        furnished.add("semi furnished")
        furnished.add("Show All")
        sortlistingsby.add("Price lowest")
        sortlistingsby.add("Price highest")
        sortlistingsby.add("medium")


       val  dist = findViewById<View>(R.id.spinner) as Spinner
      dist.setAdapter(ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, distance))
        val  price = findViewById<View>(R.id.spinner7) as Spinner
        price.setAdapter(ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, pricebetween))
        val  price1 = findViewById<View>(R.id.spinner8) as Spinner
        price1.setAdapter(ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, pricebetween1))
        val  bed = findViewById<View>(R.id.spinner2) as Spinner
        bed.setAdapter(ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, bedrooms))
        val  bed1 = findViewById<View>(R.id.spinner3) as Spinner
        bed1.setAdapter(ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, bedrooms1))
        val  furnish = findViewById<View>(R.id.spinner4) as Spinner
        furnish.setAdapter(ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, furnished))
        val  sort = findViewById<View>(R.id.spinner5) as Spinner
        sort.setAdapter(ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sortlistingsby))
    }
}
