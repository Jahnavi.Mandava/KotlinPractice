open class Animal{
var color:String=""
    fun eat(){
      println("Eat")
    }
}
class Dogg: Animal(){
    var breed:String=""
    fun bark(){
        println("Bark")
    }

}
class Cat: Animal(){

    fun meow(){
        println("Meow")
    }
}
fun main(args:Array<String>){
    var dogg = Dogg()
    dogg.breed="labra"
    dogg.color="black"
    dogg.bark()
    dogg.eat()

    var cat = Cat()

    cat.color="brown"
    cat.meow()
    cat.eat()

    var animal=Animal()
    animal.color="white"
    animal.eat()
}