//primary and secondary constructors
class Dog(var n:String)
{
    var name:String=n
    var age:Int=0
    constructor(age:Int,name:String):this(name){

       this.age=age
    }


    fun think(){
        println("Meat is awesome....$name")
    }
}

fun main(args:Array<String>){
    var doggy = Dog(22,"doggy");
    doggy.think()
}