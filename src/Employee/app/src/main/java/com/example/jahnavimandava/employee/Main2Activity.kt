package com.example.jahnavimandava.employee

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val tv = findViewById(R.id.textView) as TextView
        val tv2 = findViewById(R.id.textView2) as TextView
        val name = findViewById(R.id.editText) as EditText
        val password = findViewById(R.id.editText2) as EditText
        val bt = findViewById(R.id.button) as Button
        val bt2 = findViewById(R.id.button2) as Button

        bt.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view:View) {
                Toast.makeText(getApplicationContext(), "Login successfull", Toast.LENGTH_SHORT).show()
            }
        })
        bt2.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view:View) {
                val it = Intent(this@Main2Activity, MainActivity::class.java)
                startActivity(it)
            }
        })
    }
}
