package com.example.jahnavimandava.employee
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import android.R.attr.password
import android.R.attr.name
import android.os.PatternMatcher
import kotlinx.android.synthetic.*
import org.intellij.lang.annotations.Pattern




class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val bt3 = findViewById(R.id.button3) as Button
        val bt4 = findViewById(R.id.button4) as Button
        val tv5 = findViewById(R.id.textView5) as TextView
        val tv6 = findViewById(R.id.textView6) as TextView
        val tv7 = findViewById(R.id.textView7) as TextView
        val tv8 = findViewById(R.id.textView8) as TextView
        val name = findViewById(R.id.editText4) as EditText
        val password = findViewById(R.id.editText5) as EditText
        val emailid = findViewById(R.id.editText6) as EditText

        bt3.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View) {

                val it = Intent(this@MainActivity, Main3Activity::class.java)
                startActivity(it)
            }
            
        })
        bt4.setOnClickListener(object:View.OnClickListener {
            override fun onClick(arg0:View) {
                // TODO Auto-generated method stub
                val it = Intent(this@MainActivity, Main2Activity::class.java)
                startActivity(it)
            }
        })
    }

}
