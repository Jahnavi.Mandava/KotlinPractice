package Collections
fun main(args:Array<String>) {
    //Arrays
    val array = arrayOf("Andhrapradesh", "Tamilnadu", "kerala")
    val mixed = arrayOf("kotlin", 17, 3.0, false)
    val numbers = intArrayOf(1, 2, 3, 4, 5)
    println(mixed[0])
    mixed[2] = 3.1415
    println(mixed[2])
    val str = "vineetha"
    println(str[0])

    val states = arrayOf("Maharashtra", "Orissa")
    val allStates = array + states

    println(allStates.size)

    val empty: Boolean = numbers.isEmpty()
    if (states.contains("madhyapradeshh")) {
        println("It contains this state")
    } else {
        println("It doesnt")
    }
    //Lists
    val arrayList = arrayListOf("jahnavi", "dhana", "roja")
    val list = arrayListOf("vineetha")
    println(arrayList[0])
    println(arrayList + list)
    println(arrayList.size)
    println(arrayList.isEmpty())
    println(arrayList.contains("dhana"))

    arrayList.add("siva")
    arrayList.add(1,"ravi")
    println(arrayList)

    val removed =arrayList.remove("dhana")
    println(arrayList)
    println(removed)

    val subList = arrayList.subList(1,3)
    println(subList)

}
