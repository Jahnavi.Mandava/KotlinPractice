abstract class Shape1(val name:String){
    abstract fun area():Double
}
class Circle1(name:String,val radius:Double):Shape1(name){
    override fun area() = Math.PI*Math.pow(radius,2.0)
}
fun main(args:Array<String>){
    val shape1 = Circle1("Large circle",17.0)
    println(shape1.area())
}  