import java.util.*;
class Person( var name:String , var age:Int): Comparable<Person>{
    override fun compareTo(other:Person):Int{
        return this.age-other.age

    }
}
fun main(args:Array<String>){
    var listOfNames=ArrayList<Person>()
    listOfNames.add(Person("janu",23))
    listOfNames.add(Person("himani",22))
    listOfNames.add(Person("thanuja",24))
    listOfNames.add(Person("kalyani",26))
    println("before sort")
    for (person in listOfNames){
        println("Name:"+person.name)
        println("age:"+person.age)
    }
    println("after sort")
    Collections.sort(listOfNames)
    for (person in listOfNames){
        println("Name:"+person.name)
        println("age:"+person.age)
    }


}