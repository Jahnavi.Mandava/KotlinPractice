open class Animal1 {
    open fun domestic() {
        println("dog is a domestic animal")
    }

    open fun wild() {
        println("lion is a wild animal")
    }

}

class Dog : Animal1() {

    override fun domestic() {
        super.domestic()
        val rows = 5
        var k = 0
        var count = 0
        var count1 = 0

        for (i in 1..rows) {
            for (space in 1..rows - i) {
                print("  ")
                ++count
            }

            while (k != 2 * i - 1) {
                if (count <= rows - 1) {
                    print((i + k).toString() + " ")
                    ++count
                } else {
                    ++count1
                    print((i + k - 2 * count1).toString() + " ")
                }

                ++k
            }
            k = 0
            count = k
            count1 = count

            println()
        }
    }

}

class Lion : Animal1() {
    override fun wild() {
        super.wild()

        val rows = 5
        var k = 0
        var count = 0
        var count1 = 0

        for (i in 1..rows) {
            for (space in 1..rows - i) {
                print("  ")
                ++count
            }

            while (k != 2 * i - 1) {
                when {
                    count <= rows - 1 -> {
                        print((i + k).toString() + " ")
                        ++count
                    }
                    else -> {
                        ++count1
                        print((i + k - 2 * count1).toString() + " ")
                    }
                }

                ++k
            }
            k = 0
            count = k
            count1 = count

            println()
        }
    }

}

fun main(args: Array<String>) {
    val dog = Dog()
    dog.domestic()
    val lion = Lion()
    lion.wild()
}